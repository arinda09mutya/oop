<?php 

require('animal.php'); //memanggil fungsi2 yg ada di animal.oho
require ('frog.php');
require('ape.php');

$animal = new Animal("shaun");

echo "Name : " .$animal->name. "<br>";
echo "Legs : " .$animal->legs. "<br>";
echo "Cold blooded : " .$animal->cold_blooded. "<br>";

echo "<br>";

$kodok = new Frog("buduk");
echo "Name : " .$kodok->name. "<br>";
echo "Legs : " .$kodok->legs. "<br>";
echo "Cold blooded : " .$kodok->cold_blooded. "<br>";
echo "Jump : " .$kodok->jump. "<br>";

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : " .$sungokong->name. "<br>";
echo "Legs : " .$sungokong->legs. "<br>";
echo "Cold blooded : " .$sungokong->cold_blooded. "<br>";
echo "Yell : " .$sungokong->yell. "<br>";
?>